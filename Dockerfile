FROM node:19.1.0-slim
COPY ./app /app
WORKDIR /app
RUN npm install
EXPOSE 3000
ENTRYPOINT ["node", "/app/server.js"]
